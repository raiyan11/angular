const gulp = require('gulp');
const runSequence = require('run-sequence');
const uglify = require('gulp-uglify');
const pump = require('pump');
const gzip = require('gulp-gzip');
const child = require('child_process');
const cssnano = require('gulp-cssnano');
const sourcemaps = require('gulp-sourcemaps');
const argv = require('yargs')
  .option('env', {
    demand: true,
    alias: 'environment',
    describe: 'Environment for angular build',
    choices: ['dev', 'staging', 'prod']
  })
  .argv;

const S3_ENDPOINT = () => {
  switch (argv.env) {
    case 'staging': {
      return 's3://stage.poured.us/';
    }
    case 'prod': {
      return 's3://admin.poured.us/';
    }
    default:
      throw Error('use `ng s` for local.');
  }
};

gulp.task('clean', function (callback) {
  child.exec(`aws --profile poured s3 rm ${S3_ENDPOINT()}`, function (e, sin, sout) {
    callback(e);
  });
});

gulp.task('version_bump', (callback) => {
  const bumpType = (argv.env === 'prod') ? 'patch' : 'prepatch';
  child.exec(`npm version ${bumpType} --no-git-tag-version`, (e, sin, sout) => callback(e));
});

gulp.task('dist', function (callback) {
  child.spawn('ng', ['build', `--env=${argv.env}`, '-sm', '--vendor-chunk=false', '--output-hashing=none', '--buildOptimizer', '--aot'], {stdio: 'inherit'})
    .on('close', callback);
});

gulp.task('minify', function (callback) {
  pump([
    gulp.src('dist/*.js'),
    uglify(),
    gulp.dest('dist'),
    gulp.src('dist/*.css'),
    sourcemaps.init(),
    cssnano(),
    sourcemaps.write('.'),
    gulp.dest('dist')
  ], callback);
});

gulp.task('compress', function (callback) {
  pump([
    gulp.src('dist/**'),
    gzip({append: false}),
    gulp.dest('dist/')
  ], callback);
});

gulp.task('deploy', function (callback) {
  child.exec(`aws --profile poured s3 sync ./dist ${S3_ENDPOINT()} --delete --metadata-directive REPLACE --cache-control 'max-age=0' --content-encoding 'gzip'`, function (err, stdout, stderr) {
    console.log(stdout, stderr);
    callback(err);
  });
});


gulp.task('build', function (callback) {
  runSequence('clean', 'version_bump', 'dist', 'minify', 'compress', 'deploy',
    function (err) {
      if (err) {
        console.log("Error!")
      } else {
        console.log('success!');
        child.spawn('aws', ["--profile", "poured", "cloudfront", "create-invalidation", "--distribution-id", "E3SBPAQ6GR1922", "--paths", "/"], {stdio: 'inherit'})
      }
    }
  )
});


